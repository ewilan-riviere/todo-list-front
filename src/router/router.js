import Vue from 'vue'
import Router from 'vue-router'

import welcome from '@/components/welcome'
import tasksList from '@/components/tasks-list'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'welcome',
    component: welcome
  },
  {
    path: '/tasks-list',
    name: 'tasks-list',
    component: tasksList
  }
]
export default new Router({
  mode: 'history',
  routes
})
