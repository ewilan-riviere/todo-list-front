import Vue from 'vue'
import router from './router/router'
import axios from 'axios'
import VueTailwind from 'vue-tailwind'
import VueCookies from 'vue-cookies'
import dotenv from 'dotenv'
import App from './App.vue'

// import eButton from './components/tools/e-button.vue'
// import globalComponents from './components/tools'

// Global style
import './css/index.css'
import './css/sass/index.scss'

dotenv.config()
Vue.use(VueTailwind)

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.api = process.env.VUE_APP_API_URL
Vue.prototype.$cookies = VueCookies

// global components
// Vue.component('e-button', eButton)
// globalComponents.forEach((component) => {
//   Vue.component(component.name, component)
// })

/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   components: { App },
//   template: '<App/>'
// })
new Vue({
  router,
  render: (h) => h(App)
}).$mount('#app')
