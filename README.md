# **TodoList · Front**

[![VueJS 2.6](https://img.shields.io/static/v1?label=VueJS&message=v2.6&color=4FC08D&style=flat-square&logo=vue.js&logoColor=4FC08D)](https://vuejs.org/)
[![TailwindCSS 1.1.4](https://img.shields.io/static/v1?label=TailwindCSS&message=v1.1.4&color=38B2AC&style=flat-square&logo=tailwind-css&logoColor=38B2AC)](https://tailwindcss.com/)
[![NodeJS 11.15](https://img.shields.io/static/v1?label=NodeJS&message=v11.15&color=339933&style=flat-square&logo=node.js&logoColor=339933)](https://nodejs.org/en)
[![Yarn 1.22](https://img.shields.io/static/v1?label=Yarn&message=v1.22&color=2C8EBB&style=flat-square&logo=yarn&logoColor=2C8EBB)](https://classic.yarnpkg.com/lang/en/)

## **Server**

[![TodoList · Back repository 1.1](https://img.shields.io/static/v1?label=TodoList-Back&message=v1.1&color=ff2d20&style=flat-square&logo=laravel&logoColor=ff2d20)](https://gitlab.com/EwieFairy/todo-list-back)  
To use this project, you have to set server with [**TodoList · Back repository**](https://gitlab.com/EwieFairy/todo-list-back).


## **1. Initialization**

```bash
# create .env
cp .env.exemple .env
```

Replace these infos:
> VUE_APP_API_URL="http://api.todo-list.localhost"

```bash
# download dependencies
yarn
# launch dev server
yarn dev
```

### ***A. Advanced***

```bash
# compile for deploy
yarn build
# run tests
yarn test
# run lint
yarn lint
```

See [Configuration Reference](https://cli.vuejs.org/config/) for more infos.

## **2. ESLint configuration**

This project embedded ESLint to check code quality. You can use it with [**Visual Studio Code**](https://code.visualstudio.com/) with below config. Use just <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd> and search `Preferences: Open Settings (JSON)` to open settings with JSON config. Add this config below current config:

```json
{
    "eslint.validate": [
        "vue",
        "html",
        "javascript",
        "typescript",
        "javascriptreact", 
        "typescriptreact"
    ],
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    }
    
}
```
